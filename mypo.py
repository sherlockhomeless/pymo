#!/usr/bin/env python3
from config import DEFAULT_TIME, DB_PATH
from mods.terminal_printer import TerminalPrinter
from mods.json_handler2 import JsonHandler2
from mods.datatypes import Timestamp2
import argparse
import signal

"""
todo:
4. Setup Config-File
6. Check if db-file is there, config, sanity-testing,...
10. clean up db
11. seperation of concernses: dont let run_timer print total time spend and such
16. [BUG] DB was deleted after shutdown while running timer -> fix, keep backup
"""

t = Timestamp2()


def parse_arguments():
    parser = argparse.ArgumentParser(description="simple pomodoro timer")
    parser.add_argument('-t', '--time', nargs='?', type=int, help='starts a timer with given minutes')
    parser.add_argument('-r', '--rest', action='store_true', help='starts a pause timer')
    parser.add_argument('-a', '--auto', action='store_true',  help='autoruns the mypo-timer with iterations of working and pausing')  # TODO: Implement
    parser.add_argument('-p', '--project', nargs='?', type=str, default="", help='adds time tracked to a certain project' )
    parser.add_argument('--testing', action='store_true', help='only here for testing of this')
    parser.add_argument('-l', '--list',  action='store_true', help='lists all projects started')
    parser.add_argument('-d', '--delete', nargs='?', type=str, default="", help="deletes the project given as the argument")
    parser.add_argument('--duty', action='store_true', help='checks if a days work is already done and one can relaxe')
    return parser.parse_args()


def get_config_dict():
    """
    returns a dictionary containing all the arguments given via cli
    """
    args = parse_arguments()
    arguments = {
        "is_autorun": args.auto,
        "timer_duration": args.time,
        "is_resting": args.rest,
        "is_testing": args.testing,
        "is_listing": args.list,
        "project": args.project,
        "delete": args.delete,
        "duty": args.duty
    }
    return arguments


def handle_termination():
    signal.signal(signal.SIGTERM, end_timer)


def main():
    # Argument Parsing
    arguments = get_config_dict()
    # add handler for receiving os-signalso
    handle_termination()
    json_handler = JsonHandler2(DB_PATH)
    # start timer depending on mode
    if arguments["delete"] != "":
        json_handler.delete_project_from_db(arguments["delete"])
        json_handler.write_db()
        TerminalPrinter.print_msg_colored(f'deleted {arguments["delete"]}\n', 'red')
        return
    elif arguments["is_autorun"]:
        while True:
            iteration = 0
            print(f"iteration {iteration}")
            TerminalPrinter.print_msg_colored("running auto\n", 'red')
            start_timer(json_handler, arguments["timer_duration"], project=arguments["project"], is_testing=arguments["is_testing"])
            start_brake(5, json_handler)
    elif arguments["is_listing"]:
        TerminalPrinter.list_all_projects(json_handler)
        return
    elif arguments["is_resting"]:
        rest_time = 5
        if type(arguments['timer_duration']) is int:
            rest_time = arguments['timer_duration']
        start_brake(rest_time, json_handler)

    else:
        start_timer(json_handler, arguments["timer_duration"], project=arguments["project"], is_testing=arguments["is_testing"], duty=arguments['duty'])


def start_brake(timer_duration, db_connection):
    start_timer(db_connection, timer_duration, is_break=True)


def start_timer(db_connection, duration, is_break=False, project=None, is_testing=False, duty=False):
    if project is not None:
        t.set_project(project)
    t.printy_duty = duty
    # sets the starting time for the data struct
    t.set_start_time()
    if duration is None:
        duration = DEFAULT_TIME
    if not is_break:
#        TerminalPrinter.run_timer_session(time, 0, db_connection, project, is_testing=is_testing)  # Print Timer
        TerminalPrinter.run_timer_session(duration, 0, db_connection, project, is_testing=is_testing)  # Print Timer
        t.stop_timer()
        if duty:
            TerminalPrinter.print_progress_for_day()
        db_connection.insert_into_db(t)
        db_connection.write_db()
    else:
        TerminalPrinter.run_timer_session(duration, 0, db_connection, None, is_break=True)  # Print Timer


# arguments necessary bc of binding to system
def end_timer(x, y):
    con = JsonHandler2(DB_PATH)
    t.stop_timer()
    print("Stoping timer!")
    print(t)
    con.insert_into_db(t)
    con.write_db()


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        end_timer(None, None)
