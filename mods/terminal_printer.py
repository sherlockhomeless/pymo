from termcolor import colored
from sys import stdout
from time import localtime, strftime, sleep
from math import floor
import os


class TerminalPrinter:
    def __init__(self, json_con):
        self.json_con = json_con
        self.project_name = None

    @staticmethod
    def print_msg(msg):
        stdout.write(msg)

    @staticmethod
    def print_msg_colored(msg, color):
        stdout.write(colored(msg, color))

    @staticmethod
    def output_time(m, s):
        if m > 10:
            stdout.write(colored(f'\r{m}:{s:02d}', 'red'))
        elif m >= 5:
            stdout.write(colored(f'\r{m:02d}:{s:02d}', 'yellow'))
        else:
            stdout.write(colored(f'\r{m:02d}:{s:02d}', 'green'))

    @staticmethod
    def run_timer_session(m, s, db_connection, project_name, is_break=False, is_testing=False):
        if is_testing:
            return
        stdout.write("started: " + strftime("%H:%M:%S\n", localtime()))
        TerminalPrinter.print_time_spent_today(db_connection)
        TerminalPrinter.print_time_spent_month(db_connection)
        TerminalPrinter.print_time_spent_on_project(db_connection, project_name)
        TerminalPrinter.print_seperator()
        while m >= 0:
            TerminalPrinter.output_time(m, s)
            s -= 1
            if s <= 0:
                s = 59
                m -= 1
            sleep(1)
        stdout.write("\n")
        if not is_break:
            os.system("notify-send \"!!!!! \nBREAK\n !!!!!\"")
        else:
            os.system("notify-send \"!!!!! \nBREAK OVER\n !!!!!\"")

    @staticmethod
    def print_time_spent_today(db_handler):
        s2day = db_handler.get_total_time_for_date()
        h2day, m2day, s2day = TerminalPrinter.seconds_to_hours_minutes_seconds(s2day)
        stdout.write(f"total time spend today: {h2day}h:{m2day}m:{s2day}s\n")

    @staticmethod
    def print_time_spent_month(db_handler):
        s_this_month = db_handler.get_total_time_for_month()
        hours_this_month, minutes_this_month, seconds_this_month = TerminalPrinter.seconds_to_hours_minutes_seconds(s_this_month)
        stdout.write(f"total time spend this month: {hours_this_month}h:{minutes_this_month}m:{seconds_this_month}s\n")

    @staticmethod
    def seconds_to_hours_minutes_seconds(seconds):
        m2day, s2day = divmod(seconds, 60)
        h2day, m2day = divmod(m2day, 60)
        return h2day, m2day, s2day

    @staticmethod
    def print_seperator():
        stdout.write("---------------\n")

    @staticmethod
    def print_time_spent_on_project(db_handler, project_name=None):
        if project_name is None:
            return
        total_time_project = db_handler.get_total_time_spent_on_project(project_name)
        h, m, s = TerminalPrinter.seconds_to_hours_minutes_seconds(total_time_project)
        if project_name == "":
            project_name = "default"
        stdout.write(f'total time spent on project {project_name}: {h}h:{m}m:{s}s\n')

    @staticmethod
    def list_all_projects(db_handler):
        all_projects = db_handler.get_all_projects()
        all_project_times_in_seconds, all_projects_in_hms = [], []

        # get duration in seconds for each project
        for elem in all_projects:
            all_project_times_in_seconds.append(db_handler.get_total_time_spent_on_project(elem))
        # convert seconds to hh:mm:ss
        for elem in all_project_times_in_seconds:
            all_projects_in_hms.append(TerminalPrinter.seconds_to_hours_minutes_seconds(elem))

        TerminalPrinter.print_seperator()
        for i in range(len(all_projects)):
            if all_projects[i] == "":
                all_projects[i] = "default"
            stdout.write(f'{all_projects[i]}: {all_projects_in_hms[i][0]}h:{all_projects_in_hms[i][1]}m:{all_projects_in_hms[i][2]}m\n')
        TerminalPrinter.print_seperator()

    def print_progress_for_day(self):
        TIMEGOAL = 60 * 6
        PROGRESS_BAR = "||||||||||||||||||||||||||||||||||||||||||||||||||"
        time_invested_toaday = self.json_con.get_total_time_for_date()
        percent_done = floor(time_invested_toaday*100/TIMEGOAL)
        stdout.write(colored(f'Progress:{floor(percent_done/2)*"|"}', 'green'))
        stdout.write(colored(f'Progress:{floor((100-percent_done)/2)*"|"}', 'red'))
