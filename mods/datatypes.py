from time import strftime, time
from math import ceil
import json
import datetime
import re


class Timestamp2:
    def __init__(self):
        self.id = None  # incrementing counter
        self.date = datetime.date.today().strftime("%Y-%m-%d")  # date of timestamp
        self.start = None  # start time in epoch
        self.start_time = None  # start time in hh-mm-ss
        self.end = None  # end time in epoc
        self.end_time = None  # end tim ein hh-mm-ss
        self.duration = None  # duration in seconds
        self.attributes = ""  # ;-seperated list of attributes
        self.printy_duty = False

    def set_start_time(self):
        self.start_time = time()
        self.start = strftime("%H:%M:%S")

    def stop_timer(self):
        self.end_time = time()
        self.end = strftime("%H:%M:%S")
        self.duration = ceil(self.end_time - self.start_time)

    def unset_project(self):
        """
        Delets PROJECT:<bla>; from Attributes of Element
        """
        # TODO: regex obj compiled for every elem, better somewhere more central
        regex_project = re.compile('PROJECT:[a-z,0-9]*;', re.IGNORECASE)
        match = regex_project.match(self.attributes)
        if match:
            begin_project_index, end_project_index = match.span()
            self.attributes = self.attributes[:begin_project_index] + self.attributes[end_project_index:]

    def set_project(self, name):
        self.attributes += f"PROJECT:{name};"
        return self

    def array(self):
        if self.attributes == "":
            return [self.date, self.start_time, self.end_time, self.duration]
        else:
            return [self.date, self.start_time, self.end_time, self.duration, self.attributes]

    def set_end_time(self, end_time):
        assert type(end_time) is str
        self.end_time = end_time

    def set_duration(self, duration):
        assert type(duration) is int
        self.duration = duration

    def get_project(self):
        """ Returns the project-name of an entry if it was set, else None """
        if 'PROJECT:' in self.attributes:
            return self.attributes.split("PROJECT:")[1].split(";")[0]
        else:
            return None

    def transform_to_dict(self):
        return self.__dict__

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    def __str__(self):
        return str(self.array())

    def __repr__(self):
        return self.__str__()
