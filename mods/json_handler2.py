import os.path

from mods.datatypes import Timestamp2
import json
from json.decoder import JSONDecodeError
import datetime

class JsonHandler2:
    """
    JSON-Handler for improved file format.
    Format is:
    [{id: <int>, date:<yyyy-mm-dd>, start:<hh-mm-ss>, end:<hh-mm-ss>, attributes:<PROJECT:BLA;...>},
    """
    def __init__(self, path):
        self.path = path
        try:
            assert os.path.isfile(path)
        except AssertionError:
            with open(self.path, 'w') as f:
                f.write('{}')
                print(f'created new db file at {self.path}')

        self.data = self.load_db()  # list of objects with format: {'attributes': '', 'duration': 4, ...}
        self.date = datetime.date.today().strftime("%Y-%m-%d")
        self.cur_year = datetime.date.today().strftime("%Y")
        self.cur_month = datetime.date.today().strftime("%m")
        self.cur_day = datetime.date.today().strftime("%d")


    def apply_action_if_condition(self, condition, action):
        """
        takes 2 functions to apply action to every element where condition returns True
        """
        assert callable(condition)
        assert callable(action)

        for elem in self.data:
            if condition(elem):
                elem = action(elem)

    def sum_if_condition(self, condition, action):
        assert callable(condition)
        assert callable(action)

        sum_up = 0
        for elem in self.data:
            if condition(elem):
                sum_up += action(elem)
        return sum_up

    def load_db(self):
        all_elems = []
        with open(self.path) as json_data:
            data_generic = json.load(json_data)
            for elem in data_generic:
                new_entry = Timestamp2()
                new_entry.id = elem["id"]
                new_entry.attributes = elem["attributes"]
                new_entry.duration = elem["duration"]
                new_entry.end = elem["end"]
                new_entry.end_time = elem["end_time"]
                new_entry.start = elem["start"]
                new_entry.start_time = elem["start_time"]
                new_entry.date = elem["date"]
                all_elems.append(new_entry)
        return all_elems

    def write_db(self):
        """
        writes the current state of self.data as a json into self.path
        """
        s = "["
        for elem in self.data:
            s += elem.to_json() + ','
        last_comma = s.rfind(',')
        list_json = list(s)
        list_json[last_comma] = ']'
        s = "".join(list_json)
        with open(self.path, 'w') as db:
            db.write(s)

    def insert_into_db(self, timestamp):
        assert type(timestamp) is Timestamp2
        self.data.append(timestamp)

    def delete_project_from_db(self, project_name):
        self.apply_action_if_condition(
            lambda elem: project_name in elem.attributes,
            lambda elem: elem.unset_project()
        )

    def get_total_time_for_month(self, year=None, month=None):
        if year is None:
            year = self.cur_year
        if month is None:
            month = self.cur_month
        sum_month = self.sum_if_condition(
            lambda elem: f'{year}-{month}' in elem.date,  # year-month has to match
            lambda elem: elem.duration)
        return sum_month

    def get_data_for_date(self, date):
        data = []
        for elem in self.data:
            if elem.date == date:
                data.append(elem)
        return data

    def get_total_time_for_date(self, date=None):
        """
        returns seconds spend on given date
        :param date: date in format of the json-file ("year-month-day")
        """
        if date is None:
            date = self.date

        return self.sum_if_condition(
            lambda elem: elem.date == date,
            lambda elem: elem.duration
        )

    def get_total_time_spent_on_project(self, project_name):
        return self.sum_if_condition(
            lambda elem: project_name in elem.attributes,
            lambda elem: elem.duration
        )

    def get_all_projects(self):
        """
        get all projects as a list of strings
        """
        all_projects = []
        for elem in self.data:
            attr = elem.attributes
            if "PROJECT:" in attr:
                project_name = attr.split("PROJECT:")[1].split(";")[0]
                if project_name not in all_projects:
                    all_projects.append(project_name)
        return all_projects

    def get_time_for_project(self, name):
        return self.sum_if_condition(
            lambda elem: name in elem.attributes,
            lambda elem: elem.duration
        )

    def _convert_old_to_new(self):
        t2_list = []
        id = 0
        for date in self.data:
            cur_date = self.data[date]
            index = 0
            while index < len(cur_date):
                new_timestamp = Timestamp2()
                new_timestamp.id = id
                id += 1
                new_timestamp.date = date
                new_timestamp.set_start_time(cur_date[index])
                index += 1
                new_timestamp.set_end_time(cur_date[index])
                index += 1
                new_timestamp.set_duration(cur_date[index])
                index += 1
                try:
                    if type(cur_date[index]) is str and "PROJECT" in cur_date[index]:
                        new_timestamp.set_project(cur_date[index].split(":")[1])
                        index += 1
                except IndexError:
                    continue
                t2_list.append(new_timestamp)
        json_string = "["
        for elem in t2_list:
            json_string += elem.to_json() + ','
        last_comma_index = json_string.rfind(",")  # replace last comma with ] bc ... },]
        json_list = list(json_string)
        json_list[last_comma_index] = ']'
        json_string = "".join(json_list)
        with open(f'{self.path}.new', 'w') as outfile:
            outfile.write(json_string)
