

class Timestamp:
    def __init__(self):
        self.start = None
        self.start_time = None
        self.end = None
        self.end_time= None
        self.duration = None
        self.project = None

    def transform_to_dict(self):
        return self.__dict__

    def start_timer(self):
        self.start_time = time()
        self.start = strftime("%H:%M:%S")

    def stop_timer(self):
        self.end_time = time()
        self.end = strftime("%H:%M:%S")
        self.duration = ceil(self.end_time - self.start_time)

    def set_project(self, name):
        self.project = f"PROJECT:{name}"

    def array(self):
        if self.project is None:
            return [self.start, self.end, self.duration]
        else:
            return [self.start, self.end, self.duration, self.project]

