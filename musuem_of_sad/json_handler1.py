import os.path
import json
import datetime
import os


class JsonHandler:
    """
    Reads and writes to the JSON-Database

    The DB has the following structure:

    [...] 2019-11-17": ["14:13:23", "14:47:51", 2069, "PROJECT:betriebssysteme", "15:09:02", "15:09:06", 4, "PROJECT:betriebssysteme", "16:06:54", "16:41:23", 2069, "PROJECT:betriebssysteme", "16:43:32", "16:43:34", 3, "PROJECT:betriebssysteme",


    """
    def __init__(self, path):
        assert os.path.isfile(path)
        self.path = path
        self.date = datetime.date.today().strftime("%Y-%m-%d")
        self.cur_year = datetime.date.today().strftime("%Y")
        self.cur_month = datetime.date.today().strftime("%m")
        self.cur_day = datetime.date.today().strftime("%d")

    def write(self, time):
        """
    writes the time into the json-time-logfile given to instance
    Structure of the JSON:
    [<date>:
        [
        {start:
        end:
        time:
        },
        ...
        ]
    ]
    :param time: time with [start, end, duration]
        """
        json_data = self.read_json_db()
        old = []
        try:
            old = json_data[self.date]
        except KeyError:
            json_data[self.date] = []
        new = old + time
        json_data[self.date] = new
        self.write_json_db(json_data)

    def delete(self, projectname):
        condition = lambda elem: projectname in elem
        action = lambda elem: "PROJECT:"
        self.apply_action_if_condition(condition, action)

    def read_json_db(self):
        """"
        returns the json-db as an object
        """
        with open(self.path) as json_data:
            return json.load(json_data)

    def write_json_db(self, json_data, is_testing=False):
        if is_testing:
            return
        with open(self.path, 'w') as outfile:
            json.dump(json_data, outfile)

    def apply_action_if_condition(self, condition, action):
        data = self.read_json_db()
        for date in data:
            cur_date = data[date]
            for i in range(len(cur_date)):
                try:
                    cur = cur_date[i]
                    if condition(cur):
                        cur_date[i] = action(cur)
                except TypeError:
                    continue
        self.write_json_db(data)

    def get_total_time_for_month(self, year=None, month=None):
        """
        :param month: month as integer with leading 0s
        :param year:  year as integer
        :return: minutes spent in month
        """
        if year is None:
            year = self.cur_year
        if month is None:
            month = self.cur_month
        sum_month = 0
        for x in range(1, 31, 1):
            search_string = f'{year}-{month}-{x:02d}'
            sum_month += self.get_total_time_for_date(search_string)
        return sum_month

    def get_total_time_for_date(self, date=None):
        """
        returns seconds spend on given date
        :param date: date in format of the json-file ("year-month-day")
        """
        if date is None:
            date = self.date
        data = self.read_json_db()
        try:
            data_today = data[date]
        except KeyError:
            return 0
        sum = 0
        for el in data_today:
            if type(el) is int:
                sum += el
        return sum

    def get_total_time_spent_on_project(self, project_name):
        data = self.read_json_db()
        sum = 0
        for date in data:
            cur_date = data[date]
            for i in range(len(cur_date)):
                try:
                    # project name is prefixed with PROJECT, the duration is the field before
                    if f'PROJECT:{project_name}' == cur_date[i]:
                        sum += cur_date[i-1]
                except TypeError:
                    continue
        return sum

    def get_all_projects(self):
        """
        :return: returns a list of all projects of the database
        """
        data = self.read_json_db()
        projects = []
        for date in data:
            cur_date = data[date]
            for elem in range(len(cur_date)):
                cur_elem = cur_date[elem]
                if type(cur_elem) is str and "PROJECT" in cur_elem and cur_elem not in projects:
                    projects.append(cur_elem)
        for i in range(len(projects)):
            projects[i] = projects[i].replace('PROJECT:', '')
        return projects

    def get_time_for_project(self, name):
        data = self.read_json_db()
        sum_for_project = 0
        for date in data:
            cur_date = data[date]
            for index in range(len(cur_date)):
                cur_elem = cur_date[index]
                if type(cur_elem) is str and cur_elem[:8] == "PROJECT":
                    sum_for_project += cur_date[index-1]
        return sum_for_project



