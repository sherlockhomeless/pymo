# mypo

A simple pomodoro timer for the terminal app with a little bit of time tracking capabilities.

## installation
* requires python3, termcolor


## usage
* ``mypo`` for starting a 35 pomodoro cycle
* ``mypo -t 25`` for starting a 25 minute cycle
* ``mypo -a`` autorunning 35min working sessions and 5min break sessions
* ``mypo -p my_project`` starting a pomodoro cycle which will be dedicated to my_project. The time will be tracked seperatly
* ``mypo -r`` take a rest period

## TODOS

[ ] include possibility to extend/shorten current time period 